"""[summary]

Returns:
    [type]: [description]
"""


def check_row(temp_list, value, row):
    """[summary]

    Args:
        temp_list ([type]): [description]
        value ([type]): [description]
        row ([type]): [description]

    Returns:
        [type]: [description]
    """

    for j in range(9):
        if temp_list[row][j] == value:
            return False
    return True


def check_col(temp_list, value, col):
    """[summary]

    Args:
        temp_list ([type]): [description]
        value ([type]): [description]
        col ([type]): [description]

    Returns:
        [type]: [description]
    """

    for i in range(9):
        if temp_list[i][col] == value:
            return False
    return True


def check_box(temp_list, value, row, col):
    """[summary]

    Args:
        temp_list ([type]): [description]
        value ([type]): [description]
        row ([type]): [description]
        col ([type]): [description]

    Returns:
        [type]: [description]
    """

    for i in range(row, row + 3):
        for j in range(col, col + 3):
            if temp_list[i][j] == value:
                return False
    return True


def check_empty_cells(temp_list, params_):
    """[summary]

    Args:
        temp_list ([type]): [description]
        params_ ([type]): [description]

    Returns:
        [type]: [description]
    """

    for i in range(9):
        if i % 3 == 0:
            params_['box_row'] = i

        for j in range(9):
            if j % 3 == 0:
                params_['box_col'] = j

            if temp_list[i][j] == 0:
                params_['row'] = i
                params_['col'] = j
                return True
    return False


def run(temp_list):
    """[summary]

    Args:
        temp_list ([type]): [description]

    Returns:
        [type]: [description]
    """

    params_ = {'row': 0, 'col': 0, 'box_row': 0, 'box_col': 0}
    if not check_empty_cells(temp_list, params_):
        return True

    for value in range(1, 10):
        if check_row(temp_list, value, params_['row']) \
                and check_col(temp_list, value, params_['col']) \
                and check_box(temp_list, value, params_['box_row'], params_['box_col']):
            temp_list[params_['row']][params_['col']] = value

            if run(temp_list):
                return True
        temp_list[params_['row']][params_['col']] = 0
    return False
